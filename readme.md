# Logic

## Total Infection
The total infection works by spreading the infection to all users connected with seed users. The algorithm is very simple; using Breadth First Search starting at seed users (nodes) and then to all nodes in the connected component which the seed user belongs to. Repeat the previous operation with all seed users. Checking the current code version of the node can tell if it's visited in the BFS and hence updated or not.


## Limited Infection Approximate
Spread infection to users as long as number of updated users less than infection limit. Assuming that there are no enormously big (in terms of size) connected components, we can update connected components one by one until we break the infection limit. Update technique is similar to Total Infection; using BFS starting at seed nodes. The difference is that we check number of updated nodes after updating each connected components and terminates if number of updated users exceeded the infection limit.


## Limited Infection Exact
This type of infection either we infect exactly `n` users or we don't infect any users at all.
1. Start by traversing the users graph identifying connected components.
2. For each connected component we save its size and a representative node.
3. Run subset sum algorithm on list of connected components sizes.
4. If a combination of connected components (cc) found with size = `n`, update those cc using representative nodes saved before.
5. Update using same BFS used in previous infection types.


# Simulation
A simple simulation class is provided. Simulation constrcuts a graph from a file with [specific format](/Simulation-File-Format). Graph visualization was planned to be added to simulation (TODO).

### Simulation File Format
+ Used format is similar to the format used in competitive programming.
+ First line contains 4 space separated integers; `n` number of users in this simulation, `a`, `b` current code base number and new code base number (after adding the new feature) respectively. The last integer is `i` infection limit used in case of limited infection. `i` is -1 in case of total infection, or ignore this value.
+ Users in simulation have ids starting from 0 to n-1.
+ The second line contains a single integer `c` number of coaches.
+ Follows `c` lines each containing multiple space separated integers. The first integer is the coach id, then follows multiple integers representing students linked with that instructor for example `3 5 2 6` indicates that user 3 coaches user 5, 2, and 6.
+ Then a line that contain a single integer `s` number of seed users that will start having the new features.
+ Followed by a single line that contains `s` space separated integers indicating the ids of seed users.


# Unit Test
Used 2 very simple Junit tests to test `TotalInfection` and `LimitedInfection`. Both unit tests are tailored to work with the graph described in `sim1.in` file.
Infection classes should be tested using some big graphs (may use some graph generators).

# Design Decisions
1. User id is integer.
2. User object is very simple (id, codeVersion, coaches, students).
3. Available users in system fit in memory.
4. `TotalInfection` and `LimitedInfectionAprox` offer starting infection at given seed users along side with starting infection without seed users.
