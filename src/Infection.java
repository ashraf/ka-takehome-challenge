import java.util.LinkedList;
import java.util.Queue;


public abstract class Infection {

	protected User[] users; // nodes
	protected int newCodeVersion;

	public Infection(User[] nodes, int newCodeVersion) {
		this.users = nodes;
		this.newCodeVersion = newCodeVersion;
	}
	
	public abstract int spread();
	
	/**
	 * update the website for the given user.
	 * 
	 * @param user
	 */
	protected void update(User user) {
		// update website version for this user
		// here we just change the code version
		user.codeVersion = newCodeVersion;
	}
	
	/**
	 * starts a Breadth first search from seed node and updates all nodes in the
	 * forest of the seed node.
	 * 
	 * @param seed
	 *            : seed user to start updating the forest from.
	 * @return number of users that have been updated.
	 */
	protected int infectForest(User seed) {
		int cnt = 0; // number of users (nodes in this forest) updated including
						// seed.
		if (seed == null)
			return cnt;

		update(seed);
		Queue<User> q = new LinkedList<User>();
		q.add(seed);
		cnt++;

		User current;
		while (!q.isEmpty()) {
			current = q.poll();

			// update all students of current user if they're not updated yet
			for (User student : current.students) {
				if (student.codeVersion < seed.codeVersion) {
					update(student);
					q.add(student);
					cnt++;
				}
			}

			// update all coaches of current user if they're not updated yet
			// bidirectional graph
			for (User coach : current.coaches) {
				if (coach.codeVersion < seed.codeVersion) {
					update(coach);
					q.add(coach);
					cnt++;
				}
			}
		}

		return cnt;
	}

	
}
