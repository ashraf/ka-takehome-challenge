
public abstract class LimitedInfection extends Infection {

	protected int infectionLimit; // approximate limit of users to be infected
	
	public LimitedInfection(User[] nodes, int newCodeVersion, int infectionLimit) {
			super(nodes, newCodeVersion);
			this.infectionLimit = infectionLimit;
	}	

}
