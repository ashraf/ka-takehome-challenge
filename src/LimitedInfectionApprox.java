
public class LimitedInfectionApprox extends LimitedInfection {

	public LimitedInfectionApprox(User[] nodes, int newCodeVersion, int infectionLimit) {
		super(nodes, newCodeVersion, infectionLimit);
	}

	@Override
	public int spread() {
		int infectCount = 0;
		User user;
		
		// update forests until number of updated users reaches the infectionlimit.
		// assuming that forests are not enormously big.
		for (int i = 0; i < users.length && infectCount < infectionLimit; i++) {
			user = users[i];
			if(user.codeVersion < newCodeVersion)
				infectCount+=infectForest(user);
		}
		return infectCount;
	}
	
	
	/**
	 * @param seeds
	 * @return number of users infected
	 */
	public int spread(int[] seeds) {
		int infectCount = 0;
		User user;
		
		// update forests until number of updated users reaches the infectionlimit.
		// assuming that forests are not enormously big.
		for (int i = 0; i < seeds.length && infectCount < infectionLimit; i++) {
			user = users[seeds[i]];
			if(user.codeVersion < newCodeVersion)
				infectCount+=infectForest(user);
		}
		return infectCount;
	}
}
