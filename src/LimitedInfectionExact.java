import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class LimitedInfectionExact extends LimitedInfection {
	
	private ArrayList<User> forests; // representative node for each connected component
	private ArrayList<Integer> forestSize; // size of each connected component.
	private byte[][] memo; // subset sum dp memo
	
	public LimitedInfectionExact(User[] nodes, int newCodeVersion,
			int infectionLimit) {
		super(nodes, newCodeVersion, infectionLimit);
	}

	@Override
	public int spread() {
		forests = new ArrayList<>();
		forestSize = new ArrayList<>();
		HashSet<User> visited = new HashSet<User>();
		
		for (User user: users) {
			if(!visited.contains(user)){
				forests.add(user);
				forestSize.add(BFS(user, visited));
			}
		}
		
		int m = forestSize.size();
		memo = new byte[infectionLimit+1][m+1];
		
		// for all memo[0][i] .. it means it's possible to infect {infectionLimit} users
		Arrays.fill(memo[0], (byte)(1));
		memo[0][m] = 0;
		
		// init all memo for n > 0 to -1 (not visited)
		for (int j = 1; j < memo.length; j++) {
			Arrays.fill(memo[j], (byte)(-1));
			memo[j][m] = 0; // to avoid out of bound index 
		}
		
		// if it's possible to infect exactly {infectionLimit} users return infectionLimit
		if(subsetSum(infectionLimit, 0))
			return infectionLimit;
		
		// if it's not possible return 0;
		return 0;
	}

	/**
	 * Starts a BFS from start node and counts the nodes in the connected component
	 * @param start node
	 * @return number of nodes in the connected component
	 */
	private int BFS(User start, HashSet<User> visited) {
		int cnt = 0; // number of (nodes in this forest) including start
		
		
		if (start == null)
			return cnt;

		Queue<User> q = new LinkedList<User>();
		visited.add(start);
		q.add(start);
		cnt++;

		User current;
		while (!q.isEmpty()) {
			current = q.poll();

			// update all students of current user if they're not updated yet
			for (User student : current.students) {
				if (!visited.contains(student)) {
					visited.add(student);
					q.add(student);
					cnt++;
				}
			}

			// update all coaches of current user if they're not updated yet
			// bidirectional graph
			for (User coach : current.coaches) {
				if (!visited.contains(coach)) {
					visited.add(coach);
					q.add(coach);
					cnt++;
				}
			}
		}

		return cnt;

	}
	
	private boolean subsetSum(int n, int i) {
		if (memo[n][i] > -1)
			return memo[n][i]==0?false:true;
		
//		if (n==0 && i == forestSize.size())
//			return true;
//		else if (i >= forestSize.size() || n < 0)
//			return false;
		
		int current = forestSize.get(i); 
		if (n >= current &&  subsetSum(n-current, i+1)){
			infectForest(forests.get(i));
			memo[n][i] = 1;
			return true;
		}else if(subsetSum(n, i+1)){
			memo[n][i] = 1;
			return true;
		}
		
		memo[n][i] = 0;
		return false;
	}

}
