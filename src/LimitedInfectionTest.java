import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;


public class LimitedInfectionTest {

	Simulator sim;
	Infection infection;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		sim = new Simulator();
		sim.create("sim1.in");
		
	}

	@Test
	public void testExactInfection() {
		infection = new LimitedInfectionExact(sim.users, sim.newCodeVersion, sim.infectionLimit);
		int cnt = infection.spread();
		
		if (cnt != sim.infectionLimit)
			fail("Number of infections is incorrect");
		
		User u;
		int infected = 0;
		System.out.print("Infected users are:");
		for (int i = 0; i < sim.users.length; i++) {
			u = sim.users[i];
			if(u.codeVersion == sim.newCodeVersion){
				System.out.print(" "+u.uID);
				infected++;
			}
		}
		System.out.println();
		
		if(infected != cnt)
			fail("actual infected users do not match with number of users to be infected");
	}

	@Test
	public void testApproxInfection() {
		infection = new LimitedInfectionApprox(sim.users, sim.newCodeVersion, sim.infectionLimit);
		int cnt = infection.spread();
		
		if (cnt < sim.infectionLimit)
			fail("Number of infections is incorrect");
		
		User u;
		int infected = 0;
		System.out.print("Infected users are:");
		for (int i = 0; i < sim.users.length; i++) {
			u = sim.users[i];
			if(u.codeVersion == sim.newCodeVersion){
				System.out.print(" "+u.uID);
				infected++;
			}
		}
		System.out.println();
		
		if(infected != cnt)
			fail("actual infected users do not match with number of users to be infected");
	}

	
}
