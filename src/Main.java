import java.io.IOException;


public class Main {

	
	
	public static void main(String[] args) {
		Simulator sim = new Simulator();
		try {
			sim.create("sim1.in");
		} catch (IOException e){
			e.printStackTrace();
		}
		
		TotalInfection infection = new TotalInfection(sim.users, sim.seedUsers, sim.newCodeVersion);
		int infected = infection.spread();
		System.out.println("Number of infected users = "+infected);
		for (int i = 0; i < sim.users.length; i++) {
			System.out.println(sim.users[i].uID+"  "+sim.users[i].codeVersion);
		}
	}

}
