import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;


public class Simulator {
	
	User users[];
	int seedUsers[];
	
	int usersNum; // number of users in this simulation
	int currentCodeVersion; // last stable code version
	int newCodeVersion; // new code version after a new feature is added
	int seedsNum; // number of seed users to start rolling out new feature. 
	int infectionLimit; // number of users to be infected
	
	public void create(String fileName) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		StringTokenizer tk = new StringTokenizer(br.readLine().trim());
		
		// reading number of users, current code version, new code version
		usersNum = Integer.parseInt(tk.nextToken());
		currentCodeVersion = Integer.parseInt(tk.nextToken());
		newCodeVersion = Integer.parseInt(tk.nextToken());
		infectionLimit = Integer.parseInt(tk.nextToken());
		
		// initializing users
		users = new User[usersNum];
		for(int i = 0 ; i < usersNum; i++)
			users[i] = new User(i, currentCodeVersion);
		
		// reading number of coaches 
		int coachesN = Integer.parseInt(br.readLine().trim());
		int coach, student;
		
		// reading coaches
		for(int i = 0 ; i < coachesN; i++){
			 tk = new StringTokenizer(br.readLine());
			 coach = Integer.parseInt(tk.nextToken());
			 while (tk.hasMoreTokens()){
				 student = Integer.parseInt(tk.nextToken());
				 // add bidirectional edge
				 users[coach].addStudent(users[student]);
				 users[student].addCoach(users[coach]);
			 }
		}
			
		seedsNum = Integer.parseInt(br.readLine());
		tk = new StringTokenizer(br.readLine());
		seedUsers= new int[seedsNum];
		for (int i = 0; i < seedsNum; i++) 
			seedUsers[i] = Integer.parseInt(tk.nextToken());
		
		br.close();
	} 
	

}
