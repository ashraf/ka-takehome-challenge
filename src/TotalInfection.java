
public class TotalInfection extends Infection{

	public TotalInfection(User[] nodes, int[] seeds, int newCodeVersion) {
		super(nodes, newCodeVersion);
	}

	/**
	 * spreads the infection to all users.
	 * 
	 * @return number of users infected.
	 */
	@Override
	public int spread() {
		int cnt = 0;

		for (User u : users)
			if (u.codeVersion < newCodeVersion)
				cnt += infectForest(u);

		return cnt;
	}
	
	/**
	 * spreads the infection among users starting from seed users.
	 * 
	 * @param seeds: ids of seed users
	 * @return number of users infected.
	 */
	public int spread(int seeds[]){
		int cnt = 0;

		for (int s : seeds)
			if (users[s].codeVersion < newCodeVersion)
				cnt += infectForest(users[s]);

		return cnt;
	}

}
