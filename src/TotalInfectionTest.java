import static org.junit.Assert.fail;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

public class TotalInfectionTest {

	Simulator sim;
	TotalInfection infection;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		sim = new Simulator();
		sim.create("sim1.in");
		infection = new TotalInfection(sim.users, sim.seedUsers, sim.newCodeVersion);
	}

	@Test
	public void testSeedInfection() {
		int cnt = infection.spread(sim.seedUsers);
		int infectedNodes[] = new int[]{0, 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14};
		
		// creating a set that contains nodes that should be infected. 
		HashSet<Integer> infected = new HashSet<Integer>();
		for (int i = 0; i < infectedNodes.length; i++) 
			infected.add(infectedNodes[i]);
		
		if (cnt != infectedNodes.length)
			fail("Number of infections is incorrect");
		
		User u;
		for (int i = 0; i < sim.users.length; i++) {
			u = sim.users[i];
			if(u.codeVersion == sim.newCodeVersion){
				if (!infected.contains(u.uID))
					fail("User "+u.uID+" shouldn't have been infected");
			} else if (infected.contains(u.uID)){
				fail("User "+u.uID+" should have been infected");
			}
			
		}
	}
	
	@Test
	public void testInfectAll() {
		int cnt = infection.spread();
		
		if (cnt != sim.usersNum)
			fail("Number of infections is incorrect");
		
		for (int i = 0; i < sim.users.length; i++) 
			if(sim.users[i].codeVersion != sim.newCodeVersion)
				fail("User "+sim.users[i].uID+" should have been infected");
	}

}
