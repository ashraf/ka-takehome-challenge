import java.util.LinkedList;
import java.util.List;


/**
 * minimal user class .. of course many other attributes should be added like
 *  badges, topics, current level, watched videos,.. etc 
 *  
 *  username and email should be used, yet here I am using a user id to label the node
 */
public class User {
	
	int uID; // user id 
	int codeVersion;
	
	// it can be one list of neighbors since the problem doesn't require directional edges
	List<User> coaches; // list of users that coach this user
	List<User> students; // list of users that this user coach
	
	
	public User(int id, int codeverion) {
		this.uID = id;
		this.codeVersion = codeverion;
		coaches = new LinkedList<>();
		students = new LinkedList<>();
	}
	
	public void addCoach(User coach){
		coaches.add(coach);
	}
	
	public void addStudent(User student){
		students.add(student);
	}
	
	public void setCodeVersion(int codeVersion){
		this.codeVersion = codeVersion;
	}
	
	public int getUID(){
		return uID;
	}
	
	public int getCodeVersion(){
		return codeVersion;
	}
	
	public List<User> getCoaches(){
		return coaches;
	}
	
	public List<User> getStudents(){
		return students;
	}
	
}
